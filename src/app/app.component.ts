import { AfterViewInit, Component, OnInit } from '@angular/core';
import gsap, { Elastic } from 'gsap';
import SplitType from 'split-type'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  
  
  title = 'portfolio';

  ngOnInit(): void {
    const text = new SplitType('#T1', { types: 'chars' });
   
  }

  ngAfterViewInit(): void {
    this.GsapTextAnimation();
  }


  GsapTextAnimation(){
    const tl1 = gsap.timeline();
    
    tl1
    .add('start')
    .to('#T1 .char', {
      y:0,
      stagger:0.05,
      delay:.2,
      duration:.1
    }, 'start')
    .to('#firstText', {
      x:-100,
      y:50,
      stagger:0.05,
      delay:0,
      duration:3
    }, 'start')


  }

}
