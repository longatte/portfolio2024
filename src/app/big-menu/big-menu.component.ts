import { Component, OnInit } from '@angular/core';
import SplitType from 'split-type';

@Component({
  selector: 'app-big-menu',
  templateUrl: './big-menu.component.html',
  styleUrls: ['./big-menu.component.scss'],
  host:{
    'class': `
    absolute overflow-hidden h-screen w-screen z-10 place-content-center flex *:opacity-0 scale-100
    transition-all duration-1000 group-[&.open]/body:scale-100 group-[&.open]/body:*:opacity-100 *:transition-all
    before:content-[\'\´] before:h-[100vh] before:w-[100vh] before:scale-0  before:absolute  before:top-0 before:bottom-0 before:m-auto before:rounded-full before:bg-indigo-900
    before:transition-all before:duration-1000 before:group-[&.open]/body:scale-[250%]   `
  }
})
export class BigMenuComponent implements OnInit {
  ngOnInit(): void {
    const Link01 = new SplitType('#Link01', { types: 'chars' });
    const Link02 = new SplitType('#Link02', { types: 'chars' });
    const Link03 = new SplitType('#Link03', { types: 'chars' });
    const Link04 = new SplitType('#Link04', { types: 'chars' });
  }

}
