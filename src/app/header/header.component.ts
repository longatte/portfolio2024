import { Component } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  host:{
    'class': 'flex fixed w-full z-20 justify-between'
  }
})
export class HeaderComponent {


  toggleSwitchValue = false;


  toggleSwitch(){
    document.body.classList.toggle("open");
    this.toggleSwitchValue = !this.toggleSwitchValue
  }

}
